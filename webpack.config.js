const webpack = require('webpack');
const path = require('path');

const production = process.env.NODE_ENV === 'production';

const entry = {
    page: [path.join(__dirname, '/dev-assets/src/page')],
};

const plugins = [
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
    }),
];

module.exports = {
    devtool: production ? 'source-map' : '#eval-source-map',
    entry,
    plugins,
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    resolve: {
        alias: {
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore',
            ),
        },
    },
    output: {
        path: path.join(__dirname, 'dist/'),
        // filename: production ? 'scripts/[name]-[hash].js' : 'scripts/[name].js',
        filename: 'scripts/[name].js',
        // chunkFilename: production ? 'scripts/[name]-[chunkhash].js' : 'scripts/[name].js',
        chunkFilename: 'scripts/[name].js',
        publicPath: '/',
    },
    serve: {
        add: (app, middleware, options) => {
            // reverse order so page loads from webpack, not from static folder
            middleware.webpack();
            middleware.content();
        },
        clipboard: false,
        content: path.resolve(__dirname, 'dev-assets'),
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                include: [
                    path.resolve('.', 'src'),
                    path.resolve('.', 'dev-assets/src'),
                ],
            },
            {
                test: /\.scss/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
            {
                test: /\.png$/,
                use: [{ loader: 'url-loader', query: { limit: 100000 } }],
            },
            { test: /\.jpg$/, loader: 'file-loader' },
        ],
    },
};
