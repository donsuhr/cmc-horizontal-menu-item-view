import $ from 'jquery';
import SubMenu from '../../src/horizontal-menu-item-view';
import './page.scss';

const superFishOptions = {
    cssArrows: false,
    speed: 'fast',
    animation: { height: 'show' },
    animationOut: { height: 'hide' },
};

const $el = $('.horizontal-menu');

const submenu = new SubMenu({
    items: ['one', 'two', 'three'],
    property: 'Product',
    unselectedText: 'Select an item',
});

$el.append(submenu.el);
$el.superfish('destroy').superfish(superFishOptions);
